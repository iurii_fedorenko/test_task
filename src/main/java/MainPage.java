import driver.Driver;

/**
 * Created by Iurii_Fedorenko on 9/18/2017.
 */
public class MainPage {

    private static final String PAGE_URL = "https://site.com/main";

    protected boolean isMainPageOpened (){
        String currentPage = Driver.getWebDriverInstance().getCurrentUrl();
        return currentPage.equals(PAGE_URL);
    }
}
