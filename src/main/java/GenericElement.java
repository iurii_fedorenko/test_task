import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Iurii_Fedorenko on 9/18/2017.
 */
public class GenericElement {
    protected WebDriver driver;
    protected By elementLocator;

    public GenericElement(WebDriver driver, By by) {
        this.driver = Driver.getWebDriverInstance();
        elementLocator = by;
    }

    public boolean isElementExist() {
        int elementQtty = Driver.getWebDriverInstance().findElements(elementLocator).size();
        switch (elementQtty){
            case 1:
                return true;
            case 0: {
                System.out.println("Element with locator: " + elementLocator.toString() + " not found");
                return false;
            }
            default: {
                System.out.println("More than one elements were found by locator: " + elementLocator.toString());
                return false;
            }
        }
    }

    public boolean isElementClickable (){
        return driver.findElement(elementLocator).isEnabled();
    }

    protected void waitElementPresent(){
        if (!isElementExist()){
            new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(elementLocator));
        }
    }

    public void clickElement(){
        if(isElementExist()&&isElementClickable()){
            driver.findElement(elementLocator).click();
        }
    }

    public void clearText(){
        waitElementPresent();
        driver.findElement(elementLocator).clear();
    }

    public String getTextBoxContent (){
        waitElementPresent();
        return driver.findElement(elementLocator).getText();
    }

    public void enterText(String text){
        waitElementPresent();
        clearText();
        driver.findElement(elementLocator).sendKeys(text);
    }
}
