package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Iurii_Fedorenko on 9/18/2017.
 */
public class Driver {
    private static final String DEFAULT_DRIVER_TYPE = "CHROME";
    private static WebDriver instance;

    private Driver(){}

    public static WebDriver getWebDriverInstance(String driverType){
        if (!isInstanceExist()) {
            DriverType type;
            try {
                type = DriverType.valueOf(driverType);
            } catch (IllegalArgumentException|NullPointerException e){
                type = DriverType.valueOf(DEFAULT_DRIVER_TYPE);
            }
            try {
                switch (type) {
                    case FIREFOX: {
                        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                        instance = new FirefoxDriver();
                        break;
                    }
                    case IE: {
                        System.setProperty("webdriver.ie.driver", "src/main/resources/IEDriverServer.exe");
                        instance = new InternetExplorerDriver();
                        break;
                    }
                    default:
                        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                        instance = new ChromeDriver();
                }
                instance.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
                instance.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                instance.manage().deleteAllCookies();
                instance.manage().window().maximize();
            } catch (RuntimeException e){
                e.printStackTrace();
            }
        } else {
            return instance;
        }
        return instance;
    }

    public static WebDriver getWebDriverInstance(){
        return getWebDriverInstance(DEFAULT_DRIVER_TYPE);
    }

    public static boolean isInstanceExist(){
        if (instance != null){
            return true;
        } else
            return false;
    }


    public static void killWebDriverInstance() {
        if (instance != null) {
            try {
                instance.quit();
                instance = null;
            } catch (WebDriverException ex) {
            }
        }
    }
}
