package driver;

/**
 * Created by Iurii_Fedorenko on 9/18/2017.
 */
public enum DriverType {
    FIREFOX, IE, CHROME;
}
