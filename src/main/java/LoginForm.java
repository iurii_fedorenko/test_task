import driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginForm {
    private static final String PAGE_URL = "https://site.com/";
    private static final By LOGIN_TEXT_BOX = By.id("inputLogin");
    private static final By PASSWORD_TEXT_BOX = By.id("inputPassword");
    private static final By LOGIN_BUTTON = By.id("btnLogin");
    private static final By CLEAR_BUTTON = By.id(" btnClear");

    public void openLoginPage() {
        if (!isLoginPageOpened()) {
            Driver.getWebDriverInstance().get(PAGE_URL);
            waitPageLoading(PAGE_URL);
        }
    }

    protected boolean isLoginPageOpened (){
        String currentPage = Driver.getWebDriverInstance().getCurrentUrl();
        return currentPage.equals(PAGE_URL);
    }

    protected void waitPageLoading (String pageURL){
        new WebDriverWait(Driver.getWebDriverInstance(), 30).until(ExpectedConditions.urlContains(pageURL));
    }

    public boolean isClearButtonExist(){
        return new GenericElement(Driver.getWebDriverInstance(), CLEAR_BUTTON).isElementExist();
    }

    public boolean isClearButtonClickable(){
        return new GenericElement(Driver.getWebDriverInstance(), CLEAR_BUTTON).isElementClickable();
    }

    public LoginForm clickClearButton(){
        new GenericElement(Driver.getWebDriverInstance(), CLEAR_BUTTON).clickElement();
        return this;
    }

    public boolean isLoginButtonExist(){
        return new GenericElement(Driver.getWebDriverInstance(), LOGIN_BUTTON).isElementExist();
    }

    public boolean isLoginButtonClickable(){
        return new GenericElement(Driver.getWebDriverInstance(), LOGIN_BUTTON).isElementClickable();
    }

    public MainPage clickLoginButton(){
        new GenericElement(Driver.getWebDriverInstance(), LOGIN_BUTTON).clickElement();
        return new MainPage();
    }

    public boolean isLoginTextboxExist(){
        return new GenericElement(Driver.getWebDriverInstance(), LOGIN_TEXT_BOX).isElementExist();
    }

    public String getLoginTextBoxContent(){
        return new GenericElement(Driver.getWebDriverInstance(), LOGIN_TEXT_BOX).getTextBoxContent();
    }

    public LoginForm enterLogin(String login){
        new GenericElement(Driver.getWebDriverInstance(), LOGIN_TEXT_BOX).enterText(login);
        return this;
    }

    public boolean isPasswordTextboxExist(){
        return new GenericElement(Driver.getWebDriverInstance(), PASSWORD_TEXT_BOX).isElementExist();
    }

    public LoginForm enterPassword(String password){
        new GenericElement(Driver.getWebDriverInstance(), PASSWORD_TEXT_BOX).enterText(password);
        return this;
    }
}
