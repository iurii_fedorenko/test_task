import driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Iurii_Fedorenko on 9/18/2017.
 */
public class TestCase {
    WebDriver driver;
    protected static final String USER_LOGIN = "test_login";
    protected static final String USER_PASSWORD= "test_password";

    @BeforeMethod(description = "Webdriver initialization")
    public void setUp(){
        driver = Driver.getWebDriverInstance();
    }

    @Test(description = "Check that page is opened")
    public void checkLoginPageOpening(){
        LoginForm loginForm = new LoginForm();
        loginForm.openLoginPage();
        Assert.assertTrue(loginForm.isLoginPageOpened());
    }

    @Test(description = "Check that all elements are present")
    public void checkAllElements(){
        LoginForm loginForm = new LoginForm();
        loginForm.openLoginPage();

        Assert.assertTrue(loginForm.isLoginButtonExist());
        Assert.assertTrue(loginForm.isClearButtonExist());
        Assert.assertTrue(loginForm.isLoginTextboxExist());
        Assert.assertTrue(loginForm.isPasswordTextboxExist());
    }

    @Test(description = "Check that 'Login' button is clickable")
    public void checkIsLoginButtonClickable(){
        LoginForm loginForm = new LoginForm();

        loginForm.openLoginPage();
        loginForm.enterLogin(USER_LOGIN).enterPassword(USER_PASSWORD);
        Assert.assertTrue(loginForm.isLoginButtonClickable());
    }

    @Test(description = "Check that user can login")
    public void checkUserLogin(){
        LoginForm loginForm = new LoginForm();

        loginForm.openLoginPage();
        loginForm.enterLogin(USER_LOGIN).enterPassword(USER_PASSWORD);
        MainPage mainPage = loginForm.clickLoginButton();

        Assert.assertTrue(mainPage.isMainPageOpened());
    }

    @Test(description = "Check that 'Clear' button is clickable")
    public void checkIsClearButtonClickable(){
        LoginForm loginForm = new LoginForm();

        loginForm.openLoginPage();
        loginForm.enterLogin(USER_LOGIN);
        Assert.assertTrue(loginForm.isLoginButtonClickable());
    }


    @Test(description = "Check that 'Clear' button clear a login")
    public void checkLoginClear(){
        LoginForm loginForm = new LoginForm();

        loginForm.openLoginPage();
        loginForm.enterLogin(USER_LOGIN).clickClearButton();
        Assert.assertEquals("", loginForm.getLoginTextBoxContent());
    }

    @AfterMethod(enabled = false)
    public void closeBrowser(){
        Driver.killWebDriverInstance();
    }
}
